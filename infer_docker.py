# keras-retinanet functions for pre-processing
from object_detector_retinanet.keras_retinanet.utils.image import read_image_bgr, preprocess_image, resize_image
from object_detector_retinanet.keras_retinanet.utils.visualization import draw_box, draw_caption
from object_detector_retinanet.keras_retinanet.utils.colors import label_color


# import for EM Merger
from object_detector_retinanet.keras_retinanet.utils import EmMerger

# matplotlib and opencv for viz
import matplotlib.pyplot as plt
import cv2
import numpy as np

# for http requests and parsing
import json
import requests


# load label to names mapping for visualization purposes
labels_to_names = {0: 'object'}


def rest_request(data):
    # TF-serving REST api runs on port 8051
    url = "http://localhost:8501/v1/models/my_dense_detector:predict"

    # convert numpy arry to bytes for json'inzig input image
    np.set_printoptions(threshold=np.inf)
    json_request = '{{ "instances" : {} }}'.format(np.array2string(data, separator=',', formatter={'float':lambda x: "%.1f" % x}))

    # infer TF-serving model
    response = requests.post(url, data=json_request)
    print('response.status_code: {}'.format(response.status_code))
    # print('response.content: {}'.format(response.content))
    return response.content  # return content


def run_inference(image_path:str):
    image = read_image_bgr(image_path)

    # copy to draw on
    draw = image.copy()
    draw = cv2.cvtColor(draw, cv2.COLOR_BGR2RGB)

    # preprocess image for network
    image = preprocess_image(image)
    image, scale = resize_image(image)

    # Run inference
    # boxes, hard_scores, labels, soft_scores = model.predict_on_batch(np.expand_dims(image, axis=0))
    rs = rest_request(np.expand_dims(image, axis=0))
    json_obj = json.loads(rs.decode('utf-8'))
    predictions = json_obj['predictions'][0]
    boxes, hard_scores, labels, soft_scores = predictions['boxes'], predictions['hard_scores'], predictions['labels'], \
                                              predictions['soft_scores']

    boxes = np.expand_dims(np.asarray(boxes), axis=0)
    hard_scores = np.expand_dims(np.asarray(hard_scores), axis=0)
    labels = np.expand_dims(np.asarray(labels), axis=0)
    soft_scores = np.expand_dims(np.asarray(soft_scores), axis=0)

    return draw, scale, (boxes, hard_scores, labels, soft_scores)


def post_process(image_path:str, model_output:tuple, scale:float,  threshold=0.3, hard_score_rate=0.3, max_detections=9999):
    boxes, hard_scores, labels, soft_scores = model_output
    soft_scores = np.squeeze(soft_scores, axis=-1)
    soft_scores = hard_score_rate * hard_scores + (1 - hard_score_rate) * soft_scores

    # correct boxes for image scale
    boxes /= scale

    # select indices which have a score above the threshold
    indices = np.where(hard_scores[0, :] > threshold)[0]

    # select those scores
    scores = soft_scores[0][indices]
    hard_scores = hard_scores[0][indices]

    # find the order with which to sort the scores
    scores_sort = np.argsort(-scores)[:max_detections]

    # select detections
    image_boxes = boxes[0, indices[scores_sort], :]
    image_scores = scores[scores_sort]
    image_hard_scores = hard_scores[scores_sort]
    image_labels = labels[0, indices[scores_sort]]
    image_detections = np.concatenate(
        [image_boxes, np.expand_dims(image_scores, axis=1), np.expand_dims(image_labels, axis=1)], axis=1)
    results = np.concatenate(
        [image_boxes, np.expand_dims(image_scores, axis=1), np.expand_dims(image_hard_scores, axis=1),
         np.expand_dims(image_labels, axis=1)], axis=1)
    filtered_data = EmMerger.merge_detections(image_path, results)
    filtered_boxes = []
    filtered_scores = []
    filtered_labels = []

    csv_data_lst = []
    csv_data_lst.append(['image_id', 'x1', 'y1', 'x2', 'y2', 'confidence', 'hard_score'])

    for ind, detection in filtered_data.iterrows():
        box = np.asarray([detection['x1'], detection['y1'], detection['x2'], detection['y2']])
        filtered_boxes.append(box)
        filtered_scores.append(detection['confidence'])
        filtered_labels.append('{0:.2f}'.format(detection['hard_score']))
        row = [image_path, detection['x1'], detection['y1'], detection['x2'], detection['y2'],
               detection['confidence'], detection['hard_score']]
        csv_data_lst.append(row)

    return filtered_boxes, filtered_scores, filtered_labels


def draw_boxes(drawing_image, filtered_boxes, filtered_scores, filtered_labels, threshold=0.3):
    for box, score, label in zip(filtered_boxes, filtered_scores, filtered_labels):
        # scores are sorted so we can break
        if score < threshold:
            break

        color = [31, 0, 255]  # label_color(label) ## BUG HERE LABELS ARE FLOATS SO COLOR IS HARDCODED

        b = box.astype(int)
        draw_box(drawing_image, b, color=color)

        caption = "{} {:.3f}".format(labels_to_names[0], score)
        draw_caption(drawing_image, b, caption)

    plt.figure(figsize=(20, 20))
    plt.axis('off')
    plt.imshow(drawing_image)
    plt.show()


if __name__ == '__main__':
    # absolute path to image
    image_path = "/home/rafay/Desktop/Ring211.jpg"

    # run inference on TF-Serving
    drawing_image, scale, model_output = run_inference(image_path)

    # post process model_output
    filtered_boxes, filtered_scores, filtered_labels = post_process(image_path, model_output, scale)

    # draw boxes
    draw_boxes(drawing_image, filtered_boxes, filtered_scores, filtered_labels)